package examples.openClosed.ex_2tiger;

/**
 * @author Jan de Rijke.
 */
public interface Animal {
	boolean sleeps();
}
