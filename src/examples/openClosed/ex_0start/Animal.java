package examples.openClosed.ex_0start;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

/**
 * @author Jan de Rijke.
 */
public class Animal {
	private Species species;



	public Animal(Species species) {
		this.species = species;
	}

	public Species getSpecies() {
		return species;
	}

	public boolean sleeps() {
		switch (species) {
			case COW: {
				int hour = LocalDateTime.now().getHour();
				return hour > 22 || hour < 6;
			}
			case BEAR:
				Month month = LocalDate.now().getMonth();
				switch (LocalDate.now().getMonth()) {
					case DECEMBER:
					case JANUARY:
					case FEBRUARY:
						return true;
					default:
						return false;
				}
			default:
				return true;
		}
	}
}
