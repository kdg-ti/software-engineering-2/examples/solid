package examples.openClosed.ex_0start;

import static examples.openClosed.ex_0start.Species.*;

/**
 * @author Jan de Rijke.
 */
public class Zoo {

	private static Animal[] animals ={new Animal(COW),new Animal(BEAR)};

	public static void main(String[] args) {
		for (Animal a : animals){
			System.out.println(a.getSpecies() +(a.sleeps()?" sleeps.":" is awake.") );
		}
	}
}