package examples.openClosed.ex_1bat;

import java.time.LocalDateTime;

/**
 * @author Jan de Rijke.
 */
public class Bat implements Animal {



	@Override
	public boolean sleeps() {
		int hour = LocalDateTime.now().getHour();
		return hour < 18 && hour > 8;
	}
}
