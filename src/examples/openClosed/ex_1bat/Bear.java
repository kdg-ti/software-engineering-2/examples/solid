package examples.openClosed.ex_1bat;

import java.time.LocalDate;

/**
 * @author Jan de Rijke.
 */
public class Bear implements Animal {


	@Override
	public boolean sleeps() {
		switch (LocalDate.now().getMonth()) {
			case DECEMBER:
			case JANUARY:
			case FEBRUARY:
				return true;
			default:
				return false;
		}
	}
}
