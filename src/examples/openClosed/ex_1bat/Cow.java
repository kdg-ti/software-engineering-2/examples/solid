package examples.openClosed.ex_1bat;

import java.time.LocalDateTime;

/**
 * @author Jan de Rijke.
 */
public class Cow implements Animal {



	@Override
	public boolean sleeps() {
		int hour = LocalDateTime.now().getHour();
		return hour > 22 || hour < 6;
	}
}
