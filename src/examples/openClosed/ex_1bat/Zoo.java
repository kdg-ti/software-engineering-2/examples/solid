package examples.openClosed.ex_1bat;

/**
 * @author Jan de Rijke.
 */
public class Zoo {
	public static void main(String[] args) {
		Animal[] animals ={new Cow(),new Bear(),new Bat()};
		for (Animal a : animals){
			System.out.println(a.getClass().getSimpleName() +(a.sleeps()?" sleeps.":" is awake."));
		}
	}
}