package examples.openClosed.ex_1bat;

/**
 * @author Jan de Rijke.
 */
public interface Animal {
	boolean sleeps();
}
