package examples.openClosed.ex_3tiger_super_Diurnal;

import java.time.LocalDate;

/**
 * @author Jan de Rijke.
 */
public class Bear implements Animal {


	@Override
	public boolean sleeps() {
		switch (LocalDate.now().getMonth()) {
			case DECEMBER:
			case JANUARY:
			case FEBRUARY:
				return true;
			default:
				return false;
		}
	}
}
