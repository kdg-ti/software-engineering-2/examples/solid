package examples.openClosed.ex_3tiger_super_Diurnal;

import java.time.LocalDateTime;

/**
 * @author Jan de Rijke.
 */
public class Diurnal implements Animal {
	@Override
	public boolean sleeps() {
		int hour = LocalDateTime.now().getHour();
		return hour > 22 || hour < 6;
	}
}