package examples.openClosed.ex_3tiger_super_Diurnal;

/**
 * @author Jan de Rijke.
 */
public interface Animal {
	boolean sleeps();
}
