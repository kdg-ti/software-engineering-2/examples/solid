package examples.openClosed.ex_3tiger_super_Diurnal;

/**
 * @author Jan de Rijke.
 */
public class Zoo {
	public static void main(String[] args) {
		Animal[] animals ={new Cow(),new Bear(),new Bat(),new Tiger()};
		for (Animal a : animals){
			System.out.println(a.getClass().getSimpleName() +(a.sleeps()?" sleeps.":" is awake."));
		}
	}
}