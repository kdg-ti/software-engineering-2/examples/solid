package examples.openClosed.ex_4tiger_delegate_sleeper;

import java.time.LocalDateTime;

/**
 * @author Jan de Rijke.
 */
public class Nocturnal implements Sleeper {



	@Override
	public boolean sleeps() {
		int hour = LocalDateTime.now().getHour();
		return hour < 18 && hour > 8;
	}
}
