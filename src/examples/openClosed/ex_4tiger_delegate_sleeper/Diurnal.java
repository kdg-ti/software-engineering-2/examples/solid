package examples.openClosed.ex_4tiger_delegate_sleeper;

import java.time.LocalDateTime;

/**
 * @author Jan de Rijke.
 */
public class Diurnal implements Sleeper {
	@Override
	public boolean sleeps() {
		int hour = LocalDateTime.now().getHour();
		return hour > 22 || hour < 6;
	}
}