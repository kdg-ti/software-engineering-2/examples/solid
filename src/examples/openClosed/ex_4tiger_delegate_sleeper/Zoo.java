package examples.openClosed.ex_4tiger_delegate_sleeper;

/**
 * @author Jan de Rijke.
 */
public class Zoo {
	public static void main(String[] args) {
		Sleeper[] animals ={new Cow(),new Bear(),new Bat(),new Tiger()};
		for (Sleeper a : animals){
			System.out.println(a.getClass().getSimpleName() +(a.sleeps()?" sleeps.":" is awake."));
		}
	}
}