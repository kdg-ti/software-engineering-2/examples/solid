package examples.openClosed.ex_4tiger_delegate_sleeper;

/**
 * @author Jan de Rijke.
 */
public class Cow  implements Sleeper {
	Sleeper delegate = new Diurnal();

	@Override
	public boolean sleeps() {
		return delegate.sleeps();
	}
}
