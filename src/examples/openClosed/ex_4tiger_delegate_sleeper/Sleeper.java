package examples.openClosed.ex_4tiger_delegate_sleeper;

/**
 * @author Jan de Rijke.
 */
public interface Sleeper {
	boolean sleeps();
}
