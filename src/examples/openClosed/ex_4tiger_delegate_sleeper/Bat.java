package examples.openClosed.ex_4tiger_delegate_sleeper;

/**
 * @author Jan de Rijke.
 */
public class Bat implements Sleeper {

	Sleeper delegate = new Nocturnal();

	@Override
	public boolean sleeps() {
		return delegate.sleeps();
	}
}
