package examples.openClosed.ex_4tiger_delegate_sleeper;

/**
 * @author Jan de Rijke.
 */
public class Bear implements Sleeper {


	Sleeper delegate = new Hibernator();

	@Override
	public boolean sleeps() {
		return delegate.sleeps();
	}
}
