package examples.openClosed.ex_4tiger_delegate_sleeper;

import java.time.LocalDate;

/**
 * @author Jan de Rijke.
 */
public class Hibernator implements Sleeper {


	@Override
	public boolean sleeps() {
		switch (LocalDate.now().getMonth()) {
			case DECEMBER:
			case JANUARY:
			case FEBRUARY:
				return true;
			default:
				return false;
		}
	}
}
